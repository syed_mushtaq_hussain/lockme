package com.simplilearn.lockme.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.security.auth.login.CredentialException;

import com.simplilearn.lockme.model.UserCredentials;
import com.simplilearn.lockme.model.Users;

public class Authentication {

	
	private static Scanner keyboard;//Input data
	private static Scanner input;
	private static Scanner lockerInput;
	
	private static PrintWriter output;//helps to write data to file
	private static PrintWriter lockerOutput;
	
	private static Users users;//model to store data
	private static UserCredentials userCredentials;
	
	
	public static void main(String[] args)  {
			
		initApp();
		welcomeScreen();
		signInOptions();
				
	}
	
	public static void signInOptions() {
		System.out.println("1. Registration");
		System.out.println("2. Login");
		System.out.println("3. Forgot Password");
		int option=keyboard.nextInt();
		
		switch (option) {
		case 1:
			registeruser();
			break;
		case 2:
			loginuser();
			break;
		default:
			System.out.println("please select 1 or 2");
			break;
		}
		
		keyboard.close();
		input.close();
	}
		
		public static void lockerOptions(String inpUsername) {
			System.out.println("1. Fetch Stored Credentials");
			System.out.println("2. Store Credentials");
			int option=keyboard.nextInt();
			
			switch (option) {
			case 1:
				fetchCredentials(inpUsername);
				break;
			case 2:
				storeCredentials(inpUsername);
				break;
			default:
				System.out.println("please select 1 or 2");
				break;
			}
			
			keyboard.close();
			input.close();
			
	}
	int options = keyboard.nextInt();
	
	public static void registeruser() {
		
			System.out.println("================================");
			System.out.println("*				*");
			System.out.println("*	Welcome to Registration Page	*");
			System.out.println("*				*");
			System.out.println("================================");		
			
			System.out.println("Enter User Name: ");
			String username=keyboard.next();
			users.setUsername(username);
			System.out.println("Enter Password: ");
			String password=keyboard.next();
			users.setPassword(password);
			
			output.println(users.getUsername());
			output.println(users.getPassword());
			System.out.println("User Registration Successful");
			output.close();
	}

	private static void loginuser() {
		System.out.println("================================");
		System.out.println("*				*");
		System.out.println("*	Welcome to Login Page	*");
		System.out.println("*				*");
		System.out.println("================================");
		
		System.out.println("Enter UserName: ");
		String inpUsername =keyboard.next();
		boolean found=false;
		while(input.hasNext() && !found) {
			if(input.next().equals(inpUsername)) {
				System.out.println("Enter Password:");
				String inpPassword = keyboard.next();
			if(input.next().equals(inpPassword)) {
				System.out.println("Login Successful !!200 ok");
				found=true;
				lockerOptions(inpUsername);
				break;}
			}
		}
	

	if (!found) {
		System.out.println("User Not Found: Login Failure 404");
	}
}
	
	public static void welcomeScreen() {
		System.out.println("================================");
		System.out.println("*				*");
		System.out.println("*	Welcome to Lockme	*");
		System.out.println("*  Your Personal Digital Locker *");
		System.out.println("*				*");
		System.out.println("================================");
		
		
	}
	//Store Credentials
	public static void storeCredentials(String loggedinuser) {
		System.out.println("================================");
		System.out.println("*				*");
		System.out.println("*	Welcome to DIGITAL LOCKER 	*");//Store your credentials
				System.out.println("*				*");
		System.out.println("================================");
		
		userCredentials.setLoggedInUser(loggedinuser);
		
		System.out.println("Enter Site name: ");
		String siteName=keyboard.next();
		userCredentials.setSiteName(siteName);
		
		System.out.println("Enter User Name: ");
		String username=keyboard.next();
		userCredentials.setUsername(username);
		
		System.out.println("Enter Password: ");
		String password=keyboard.next();
		userCredentials.setPassword(password);
		
		lockerOutput.println(userCredentials.getLoggedInUser());
		lockerOutput.println(userCredentials.getSiteName());
		lockerOutput.println(userCredentials.getUsername());
		lockerOutput.println(userCredentials.getPassword());
		
		
		System.out.println("your Credentials are saved Successfully");
		lockerOutput.close();
	}
	
	
	//Fetch Credentials 
	
	public static void fetchCredentials(String inpUsername) {
		System.out.println("================================");
		System.out.println("*				*");
		System.out.println("*	Welcome to Digital Locker	*");
		System.out.println("*	Your Credentials are 		*");
		System.out.println("*				*");
		System.out.println("================================");
		System.out.println(inpUsername);		
		while(lockerInput.hasNext()) {
			if(lockerInput.next().equals(inpUsername)) {
				System.out.println("Site Name:" + lockerInput.next());
				System.out.println("User Name:" + lockerInput.next());
				System.out.println("Password:" + lockerInput.next());
				}
			}
		}
	

	
	
public static void initApp() {
	
	File dbfile=new File("database.txt");
	File lockerfile=new File("locker-file.txt");
	
	
	try {
		input = new Scanner(dbfile);//Read Data from file
		lockerInput = new Scanner(lockerfile); //Read data from locker file
		keyboard= new Scanner(System.in);//Read Data from keyboard
		
		
		//output
		output = new PrintWriter(new FileWriter(dbfile,true));
		lockerOutput = new PrintWriter(new FileWriter(lockerfile,true));

		users = new Users();
		userCredentials = new UserCredentials();
		
		
		
	} catch (IOException e) {
	System.out.println("404: File Not Found");
	}
	
}
}
